package com.aprender.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MiPrimerMicroserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(MiPrimerMicroserviceApplication.class, args);
	}

}
