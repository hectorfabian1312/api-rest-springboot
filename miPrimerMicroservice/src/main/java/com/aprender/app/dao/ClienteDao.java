package com.aprender.app.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import com.aprender.app.entity.Cliente;

public interface ClienteDao extends JpaRepository<Cliente, Long>{

	
}
