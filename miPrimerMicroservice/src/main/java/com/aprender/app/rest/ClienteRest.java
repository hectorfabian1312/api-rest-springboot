package com.aprender.app.rest;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.aprender.app.dao.ClienteDao;
import com.aprender.app.entity.Cliente;

@RestController
@RequestMapping("/clientes")
public class ClienteRest {
	
	@Autowired
	private ClienteDao clienteDao;
	
	@GetMapping("/hola")
	public String holaMundo() {
		return "Hola mundo desde spring con micro services";
	}
	
	@GetMapping
	public ResponseEntity<List<Cliente>> getClientes(){
		List<Cliente> clientes = clienteDao.findAll();
		return ResponseEntity.ok(clientes);
	}
	
	@GetMapping("/{idClente}")
	public ResponseEntity<Cliente> getCliente(@PathVariable("idClente") Long idcliente) {
		Optional<Cliente> cliente = clienteDao.findById(idcliente);
		if (cliente.isPresent()) {
			return ResponseEntity.ok(cliente.get());
		}else {
			return ResponseEntity.noContent().build();
		}
	}
	
	@PostMapping
	public ResponseEntity<Cliente> saveCliente(@RequestBody Cliente cliente){
		Cliente clienteNuevo = clienteDao.save(cliente);
		return ResponseEntity.ok(clienteNuevo);
	}
	
    @DeleteMapping("/{idCliente}")
	public ResponseEntity<Void> deleteCliente(@PathVariable("idCliente") Long idCliente){
		clienteDao.deleteById(idCliente);
		return ResponseEntity.ok(null);
	}

    @PutMapping
	public ResponseEntity<Cliente> updateCliente(@RequestBody Cliente cliente) {

		Optional<Cliente> client = clienteDao.findById(cliente.getId());

		if (client.isPresent()) {

			Cliente updateCliente = client.get();
			updateCliente.setNombre(cliente.getNombre());
			updateCliente.setTelefono(cliente.getTelefono());
			clienteDao.save(updateCliente);
			return ResponseEntity.ok(updateCliente);
		} else {
			return ResponseEntity.notFound().build();
		}

	}
    
}
